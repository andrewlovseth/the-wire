<?php get_header(); ?>
	
    <article>

		<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>				
    	
			<section class="featured-image cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
			</section>

			<section class="article-header">
				<div class="wrapper">

					<h1><?php the_title(); ?></h1>
					<span class="date">Posted <?php the_date('F j, Y'); ?></span>

					<div class="details">
						<span class="type"><?php the_field('type'); ?></span>

						<?php $locations = wp_get_post_terms($post->ID, 'location'); if($locations): ?>
							<?php foreach($locations as $location): ?>
								<span class="location"><?php echo $location->name; ?></span>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>

					<div class="cta">
						<a href="<?php the_field('apply_link'); ?>" class="btn" rel="external">Apply for Job</a>
					</div>

				</div>
			</section>

			<section class="article-body">
				<div class="wrapper">
				
					<?php the_field('job_description'); ?>

				</div>
			</section>

			<section class="article-footer">
				<div class="wrapper">

					<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						<div class="author">
							<div class="image">
								<img src="<?php $image = get_field('photo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h3><?php the_title(); ?> <a href="mailto:<?php the_field('email'); ?>" class="email">Email</a></h3>
								<?php the_content(); ?>
							</div>
						</div>
						
					<?php wp_reset_postdata(); endif; ?>

				</div>
			</section>

		<?php endwhile; endif; ?>

    </article>


<?php get_footer(); ?>