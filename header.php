<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<script src="https://use.typekit.net/cny1bbi.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php the_field('head_html', 'options'); ?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<?php get_template_part('partials/survey'); ?>

	<header>
		<div class="wrapper">

			<div id="header-logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<nav>
				<?php wp_nav_menu( array( 'menu' => 'Main Menu') ); ?>

				<div id="header-search">
					<?php get_search_form(); ?>
				</div>
			</nav>

		</div>
	</header>