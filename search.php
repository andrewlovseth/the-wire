<?php get_header(); ?>

	<section id="posts-archive" class="search">
		<div class="wrapper">
		
			<?php get_template_part('partials/filters'); ?>

			<div class="archive-header">
				<h1>Search Results: <?php echo get_search_query(); ?></h1>
			</div>

			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<?php get_template_part('partials/archive-article'); ?>						

					<?php endwhile; ?>

				</div>

			<?php else: ?>

				<div class="no-posts">
					<h2>No results found.</h2>
					<p>Please try another search.</p>

				</div>

			<?php endif; ?>

		</div>
	</section>
	
<?php get_footer(); ?>