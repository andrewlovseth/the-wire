	<footer>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<?php the_field('footer_info', 'options'); ?>

			</div>

			<?php if(have_rows('link_columns', 'options')): while(have_rows('link_columns', 'options')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'column' ): ?>
					
					<div class="column">

			    		<h4><a href="<?php the_sub_field('head_link'); ?>"><?php the_sub_field('head_label'); ?></a></h4>

			    		<?php if(have_rows('links')): ?>
			    			<ul>
					    		<?php while(have_rows('links')): the_row(); ?>
					 
								    <li>
								        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
								    </li>

								<?php endwhile; ?>
							</ul>
						<?php endif; ?>

					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</footer>
	

	<?php if(is_post_type_archive('tribe_events')): ?>
		<?php wp_footer(); ?>
	<?php endif; ?>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>

	<?php if(!is_post_type_archive('tribe_events')): ?>
		<?php wp_footer(); ?>
	<?php endif; ?>


	<?php the_field('footer_html', 'options'); ?>

</body>
</html>