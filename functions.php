<?php



/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );





/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);


/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function replace_admin_menu_icons_css() {
    ?>
    <style>

		.dashicons-admin-post:before,
		.dashicons-format-standard:before {
		    content: "\f331";
		}

    </style>
    <?php
}



function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('post'));
    }
 
	return $query;
}
 
add_filter('pre_get_posts','searchfilter');


function tagfilter($query) {
 
    if ($query->is_tag && !is_admin() ) {
        $query->set('post_type',array('post'));
    }
 
    return $query;
}
 
add_filter('pre_get_posts','tagfilter');




/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/



function my_relationship_query( $args, $field, $post_id ) {

    $args['orderby'] = 'date';
    $args['order'] = 'DESC';

    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') ) {
    acf_add_options_sub_page('Global');
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');






//Enqueue Ajax Scripts
function enqueue_ajax_filter_scripts() {
    wp_register_script( 'ajax-filter-js', get_bloginfo('template_url') . '/js/ajax-filter.js', '', '1.0', true );
    wp_localize_script( 'ajax-filter-js', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    wp_enqueue_script( 'ajax-filter-js' );
}
add_action('wp_enqueue_scripts', 'enqueue_ajax_filter_scripts');

//Add Ajax Actions
add_action('wp_ajax_listing_filter', 'ajax_listing_filter');
add_action('wp_ajax_nopriv_listing_filter', 'ajax_listing_filter');


function ajax_listing_filter() {
    $query_data = $_GET;
    
    $location_term = $query_data['location'];
    $all_location = array();
    $locations = get_terms( array(
        'taxonomy' => 'location',
        'hide_empty' => false
    ));
    foreach( $locations as $location ) {
        array_push($all_location, $location->slug);
    }


    $department_term = $query_data['dept'];
    $all_depts = array();
    $depts = get_terms( array(
        'taxonomy' => 'dept',
        'hide_empty' => false
    ));
    foreach( $depts as $dept ) {
        array_push($all_depts, $dept->slug);
    }



    $tax_query = array('relation' => 'AND');

    if ($location_term != '*') {
        $tax_query[] =  array(
            'taxonomy' => 'location',
            'field'    => 'slug',
            'terms'    => array($location_term),
        );
    } else {
        $tax_query[] =  array(
            'taxonomy' => 'location',
            'field'    => 'slug',
            'terms'    => $all_location,
        );
    }

    if ($department_term != '') {
        $tax_query[] =  array(
            'taxonomy' => 'dept',
            'field'    => 'slug',
            'terms'    => array($department_term),
        );
    } else {
        $tax_query[] =  array(
            'taxonomy' => 'dept',
            'field'    => 'slug',
            'terms'    => $all_depts,
        );
    }


    $date_status = $query_data['date'];
    if($date_status == 'oldest') {
        $dateOrder = 'ASC';
    } else {
        $dateOrder = 'DESC';
    }





    
    $listing_args = array(
        'post_type' => 'jobs',
        'posts_per_page' => 150,
        'tax_query' => $tax_query,
        'orderby' => 'date',
        'order' => $dateOrder
    );

    $listing_loop = new WP_Query($listing_args);

    if( $listing_loop->have_posts() ):
        while( $listing_loop->have_posts() ): $listing_loop->the_post();
            get_template_part('partials/job');
        endwhile;       
    else:
        get_template_part('partials//job-none');
    endif;
    wp_reset_postdata();
    
    die();
}
