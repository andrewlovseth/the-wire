<?php get_header(); ?>

	<section id="posts-archive" class="tag">
		<div class="wrapper">
		
			<?php get_template_part('partials/filters'); ?>

			<div class="archive-header">
				<h1><?php single_tag_title('Tag: '); ?></h1>
			</div>

			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<?php get_template_part('partials/archive-article'); ?>						

					<?php endwhile; ?>

				</div>

			<?php endif; ?>
			
			<?php get_template_part('partials/pagination'); ?>		

		</div>
	</section>
	
<?php get_footer(); ?>