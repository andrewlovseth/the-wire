<?php get_header(); ?>
	
    <article>

		<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>				
    	
			<section class="article-header">
				<div class="wrapper">

					<?php the_category(''); ?>
					<h1><?php the_title(); ?></h1>
					<span class="date">Posted <?php the_date('F j, Y'); ?></span>

				</div>
			</section>

			<section class="featured-image cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
			</section>
	    	
		    <?php get_template_part('partials/article-body'); ?>

		    <?php get_template_part('partials/article-footer'); ?>

		<?php endwhile; endif; ?>

    </article>

    <?php if(get_field('key_color')): ?>

    	<style type="text/css">

    		body.single-post article section.article-body blockquote:before {
    			background: <?php echo get_field('key_color'); ?> !important;
    		}

    	</style>

	<?php endif; ?>

<?php get_footer(); ?>