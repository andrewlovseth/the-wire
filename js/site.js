
$(document).ready(function() {

	// rel="external"
	$('a[rel="external"], #menu-item-23 a').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('nav').slideToggle(300);
		return false;
	});

	// Smooth Scroll
	$('body#clearlink-guide #guide-links a').smoothScroll();
	$('.content-section .btn').smoothScroll();
	$('#back-to-top').smoothScroll();


	// Home Hero Carousel
	$('body.home #hero').slick({
		dots: true,
		arrows: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1
	});

  $('#job-types-wrapper a.dept-toggle').on('click', function(){
      $('#job-types-wrapper article a').removeClass('active');

      var article = $(this).closest('article');
      var articleLinks = article.find('a.dept-toggle');
      articleLinks.addClass('active');

      return false;
  });


  $('.overlay .close').on('click', function(){
    var overlay = $(this).closest('.overlay');

    overlay.fadeOut(200);

    return false;
  });

  var modalName = $('.overlay .content-wrapper').data('survey-name');
  var localStorageName = modalName + '-visited'

  if(localStorage[localStorageName]!='yes'){    // This is the check
    $('.overlay').show();
  }

// Write the localStorage value for next visit...
localStorage[localStorageName] =  'yes';



	// Home More Posts Carousel
	$('body.home #more .posts').slick({
		dots: true,
		arrows: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	      }
	    }
		]
	});

	// Fixed on Scroll Guide Nav
	$('body#clearlink-guide #guide-links').scrollToFixed();


	$('a.search-toggle').click(function(){
		$('.text-search').toggleClass('flex');

		return false;

	});



  // the input field
  var $input = $("input[type='search']"),
    // clear button
    $clearBtn = $("button[data-search='clear']"),
    // prev button
    $prevBtn = $("button[data-search='prev']"),
    // next button
    $nextBtn = $("button[data-search='next']"),
    // the context where to search
    $content = $(".content-section, .guide-section"),
    // jQuery object to save <mark> elements
    $results,
    // the class that will be appended to the current
    // focused element
    currentClass = "current",
    // top offset for the jump (the search bar)
    offsetTop = 200,
    // the current index of the focused element
    currentIndex = 0;

  /**
   * Jumps to the element matching the currentIndex
   */
  function jumpTo() {
    if ($results.length) {
      var position,
        $current = $results.eq(currentIndex);
      $results.removeClass(currentClass);
      if ($current.length) {
        $current.addClass(currentClass);
        position = $current.offset().top - offsetTop;
        window.scrollTo(0, position);
      }
    }
  }

  /**
   * Searches for the entered keyword in the
   * specified context on input
   */
  $input.on("input", function() {
  	var searchVal = this.value;
    $content.unmark({
      done: function() {
        $content.mark(searchVal, {
          separateWordSearch: true,
          done: function() {
            $results = $content.find("mark");
            currentIndex = 0;
            jumpTo();
          }
        });
      }
    });
  });

  /**
   * Clears the search
   */
  $clearBtn.on("click", function() {
    $content.unmark();
    $input.val("").focus();
  });

  /**
   * Next and previous search jump to
   */
  $nextBtn.add($prevBtn).on("click", function() {
    if ($results.length) {
      currentIndex += $(this).is($prevBtn) ? -1 : 1;
      if (currentIndex < 0) {
        currentIndex = $results.length - 1;
      }
      if (currentIndex > $results.length - 1) {
        currentIndex = 0;
      }
      jumpTo();
    }
  });





});



var sections = $('section.guide-section')
  , nav = $('#guide-links')
  , nav_height = nav.outerHeight();
 
$(window).on('scroll', function () {
	var cur_pos = $(this).scrollTop();

	sections.each(function() {
		var top = $(this).offset().top - nav_height,
		    bottom = top + $(this).outerHeight();

		if (cur_pos >= top && cur_pos <= bottom) {
		  nav.find('a').removeClass('active');
		  sections.removeClass('active');

		  $(this).addClass('active');
		  nav.find('a[href="/clearlink-guide/#'+$(this).attr('id')+'"]').addClass('active');
		}
	});


	if ($('body').hasClass('page-id-9')) {
		if($(this).scrollTop()>=$('#section-content').position().top){
			$('a#back-to-top').addClass('fixed');
		} else {
			$('a#back-to-top').removeClass('fixed');
		}
	}

});