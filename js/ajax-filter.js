//Genre Ajax Filtering
jQuery(function($)
{
	//Load posts on page load
	listing_get_posts();

	//Find Selected Location
	function getSelectedLocation() {
		var location = $('#location-selector').find(":selected").val();	
		return location;
	}

	//Find Selected Date
	function getSelectedDate() {
		var location = $('#date-selector').find(":selected").val();	
		return location;
	}

	//Find Selected Department
	function getSelectedDept() {
		var dept = $('#job-types-wrapper').find("a.see-all.active.dept-toggle").data('dept');	
		return dept;
	}

	//If select is changed, load posts
	$('select.filter').on('change', function(){
		listing_get_posts(); //Load Posts
	});

	$('#job-types-wrapper a.dept-toggle').on('click', function(){
		listing_get_posts(); //Load Posts

		var deptName = $(this).closest('article').find('h3 a').text();
		$('#job-list h3.cat-header').text(deptName);
	});
	
	//Main ajax function
	function listing_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'listing_filter',
				location: getSelectedLocation,
				dept: getSelectedDept,
				date: getSelectedDate
			},
			beforeSend: function ()
			{
				$('#loader').css('visibility', 'visible');
			},
			success: function(data)
			{
				//Hide loader here
				$('#loader').css('visibility', 'hidden');
				$('#response').html(data);
			},
			error: function()
			{
				$("#response").html('<p>There has been an error</p>');
			}
		});				
	}



	
});