<?php get_header(); ?>

	<section id="posts-archive" class="popular">
		<div class="wrapper">
		
			<?php get_template_part('partials/filters'); ?>

			<div class="archive-header">
				<h1>Most Popular</h1>
			</div>

			<?php

			    if (function_exists('wpp_get_mostpopular')) {

					$args = array(
					    'wpp_start' => '<div class="top-posts">',
					    'wpp_end' => '</div>',
						'post_type' => 'post',
						'stats_date' => 1,
						'excerpt_length' => 140,
					    'thumbnail_width' => 100,
					    'thumbnail_height' => 240,
					    'post_html' => '<article><div class="info"><h3>{title}</h3></div></article>'
					);

			    	wpp_get_mostpopular($args);
			    }			        
			?>


		</div>
	</section>
	
<?php get_footer(); ?>