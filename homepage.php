<?php

/*

	Template Name: Home

*/

get_header(); ?>


	<section id="hero">

		<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')): the_row(); ?>

			<?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

				<article<?php $template = get_page_template_slug($post->ID); if($template == 'video-post.php'): ?> class="video"<?php endif; ?>>
					<div class="blurred-bg cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);"></div>	 
					<div class="wrapper">

						<div class="info">
							<?php the_category(''); ?>
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<?php the_field('teaser'); ?>
							<a href="<?php the_permalink(); ?>" class="btn">Continue Reading</a>

						</div>

						<a href="<?php the_permalink(); ?>" class="image cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
							<?php if($template == 'video-post.php'): ?>
								<span class="play"></span>
							<?php endif; ?>
						</a>
					</div>
				</article>

			<?php wp_reset_postdata(); endif; ?>

		<?php endwhile; endif; ?>

	</section>


	<section id="guide-links">
		<div class="wrapper">

			<h5><a href="<?php echo site_url('clearlink-guide'); ?>">Explore the Clearlink Guide</a></h5>

			<?php wp_nav_menu( array( 'menu' => 'Guide Links') ); ?>

		</div>
	</section>



	<section id="latest">
		<div class="wrapper">

			<h2>Latest Posts</h2>

			<div class="posts">

				<?php $exclude_ids = array(); ?>

				<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')): the_row(); ?>

					<?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						<?php $post_object = get_sub_field('post'); $exclude_ids[] = $post_object->ID; ?>

					<?php wp_reset_postdata(); endif; ?>

				<?php endwhile; endif; ?>

				<?php $longform = get_field('featured_longform_post'); $exclude_ids[] = $longform->ID; ?>


				<?php if(have_rows('more_featured_posts')): while(have_rows('more_featured_posts')): the_row(); ?>

					<?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						<?php $post_object = get_sub_field('post'); $exclude_ids[] = $post_object->ID; ?>

					<?php wp_reset_postdata(); endif; ?>

				<?php endwhile; endif; ?>
	
				<?php

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 4,
						'post__not_in' => $exclude_ids
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<article<?php $template = get_page_template_slug($post->ID); if($template == 'video-post.php'): ?> class="video"<?php endif; ?>>
						<div class="image">
							<a href="<?php the_permalink(); ?>">
								<?php if($template == 'video-post.php'): ?>
									<span class="play"></span>
								<?php endif; ?>

								<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>
						
						<div class="info">
							<?php the_category(''); ?>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>

		</div>
	</section>


	<section id="longform">

		<?php
			$explicit_feature = get_field('featured_longform_image');
		?>


		<?php $post_object = get_field('featured_longform_post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

			<?php 
				if($explicit_feature) {
					$featured_image = $explicit_feature;
				} else {
					$featured_image = get_field('featured_image');
				}
			?>

			<article class="cover" style="background-image: url(<?php echo $featured_image['url']; ?>);">
				<div class="wrapper">

					<div class="info">
						<h4>Longform Content</h4>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<a href="<?php the_permalink(); ?>" class="btn">Continue Reading</a>
					</div>
				</div>
			</article>

		<?php wp_reset_postdata(); endif; ?>

	</section>


	<section id="more">

			<?php if(have_rows('more_featured_posts')): ?>

				<div class="posts">

					<?php while(have_rows('more_featured_posts')): the_row(); ?>

						<?php $post_object = get_sub_field('post'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<article<?php $template = get_page_template_slug($post->ID); if($template == 'video-post.php'): ?> class="video"<?php endif; ?>>
								<a href="<?php the_permalink(); ?>" class="image cover" style="background-image: url(<?php $image = get_field('featured_image'); echo $image['url']; ?>);">
									<?php if($template == 'video-post.php'): ?>
										<span class="play"></span>
									<?php endif; ?>
								</a>


								<div class="info">
									<?php the_category(''); ?>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>
							</article>

						<?php wp_reset_postdata(); endif; ?>

					<?php endwhile; ?>

				</div>

			<?php endif; ?>

	</section>


<?php get_footer(); ?>