<section class="pagination">
	<div class="prev">
		<?php previous_posts_link(); ?>
	</div>

	<div class="next">
		<?php next_posts_link(); ?>
	</div>
</section>