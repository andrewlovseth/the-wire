<div class="filter-wrapper">
     <h5>Location</h5>
     <select class="filter" id="location-selector">
          <option value="*">All Locations</option>

          <?php
               $locations = get_terms( array(
                    'taxonomy' => 'location',
                    'hide_empty' => true
               ));
          ?>

          <?php foreach( $locations as $location ): ?>
               <option class="" value="<?php echo $location->slug; ?>"><?php echo $location->name; ?></option>
          <?php endforeach; ?>

     </select>
</div>