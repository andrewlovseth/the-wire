<section class="inset-box">
	<div class="wrapper">
        
        <div class="inset-wrapper">
        	<?php the_sub_field('content'); ?>
        </div>

    </div>
</section>