<section class="two-column-text-and-photo">
	<div class="wrapper">

		<?php if(have_rows('articles')): ?>

				<?php while(have_rows('articles')): the_row(); ?>
		 
				    <article>
				    	<div class="info">
					        <h4 class="key-color"><?php the_sub_field('headline'); ?></h4>
					        <?php the_sub_field('deck'); ?>
					    </div>

					    <div class="photo">
					    	<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    </div>
				    </article>

				<?php endwhile; ?>

		<?php endif; ?>

    </div>
</section>