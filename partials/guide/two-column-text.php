<section class="two-column-text">
	<div class="wrapper">

		<?php if(have_rows('articles')): ?>

			<div class="articles">
				<?php while(have_rows('articles')): the_row(); ?>
		 
				    <article>
				        <h4 class="key-color"><?php the_sub_field('headline'); ?></h4>
				        <?php the_sub_field('deck'); ?>
				    </article>

				<?php endwhile; ?>
			</div>

		<?php endif; ?>

    </div>
</section>