<section class="header key-background-color">
	<div class="wrapper">

        <h2><?php the_title(); ?></h2>
        <?php the_sub_field('deck'); ?>

    </div>
</section>