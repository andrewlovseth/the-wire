<section class="full-width-headline">
	<div class="wrapper">

        <h3><?php the_sub_field('headline'); ?></h3>

    </div>
</section>