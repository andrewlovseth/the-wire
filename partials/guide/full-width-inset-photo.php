<section class="full-width-inset-photo">
	<div class="wrapper">

		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

    </div>
</section>