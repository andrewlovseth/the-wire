<section class="full-width-text">
	<div class="wrapper">

		<?php if(get_sub_field('headline')): ?>
	        <h3 class="key-color"><?php the_sub_field('headline'); ?></h3>
	    <?php endif; ?>

		<?php if(get_sub_field('deck')): ?>
	        <?php the_sub_field('deck'); ?>
	    <?php endif; ?>

    </div>
</section>