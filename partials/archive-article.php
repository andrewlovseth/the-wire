<article<?php $template = get_page_template_slug($post->ID); if($template == 'video-post.php'): ?> class="video"<?php endif; ?>>
	<div class="image">
		<a href="<?php the_permalink(); ?>">
			<?php if($template == 'video-post.php'): ?>
				<span class="play"></span>
			<?php endif; ?>

			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>
	
	<div class="info">
		<?php the_category(''); ?>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

	</div>
</article>