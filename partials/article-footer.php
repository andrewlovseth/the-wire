<section class="article-footer">
	<div class="wrapper">

		<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

			<div class="author">
				<div class="image">
					<img src="<?php $image = get_field('photo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="info">
					<h3><?php the_title(); ?> <a href="mailto:<?php the_field('email'); ?>" class="email">Email</a></h3>
					<?php the_content(); ?>
				</div>
			</div>
			
		<?php wp_reset_postdata(); endif; ?>

		<?php if(has_tag()): ?>
			<div class="tags">
				 <?php the_tags( '', ' ', '' ); ?> 
			</div>
		<?php endif; ?>
		
		<div class="disqus-comments">
			<?php comments_template(); ?> 
		</div>

	</div>
</section>