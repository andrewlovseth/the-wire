<article>

	<?php $locations = wp_get_post_terms($post->ID, 'location'); if($locations): ?>
		<?php foreach($locations as $location): ?>
			<h4><?php echo $location->name; ?></h4>
		<?php endforeach; ?>
	<?php endif; ?>

	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

	<div class="info">
		<div class="description">
			<?php the_field('short_description'); ?>
		</div>

		<div class="cta">
			<a href="<?php the_permalink(); ?>" class="btn">See Details</a>
		</div>
	</div>

</article>