<?php if(is_front_page()): ?>

	<?php if(get_field('display_survey', 'options') == true): ?>

		<div class="overlay">
			<div class="wrapper">

				<div class="content">
					<a href="#" class="close">X</a>

					<div class="content-wrapper" data-survey-name="<?php echo sanitize_title_with_dashes( get_field('survey_name', 'options') ); ?>">
						<?php echo do_shortcode(get_field('survey_shortcode', 'options')); ?>
					</div>
				</div>

			</div>
		</div>

	<?php endif; ?>

<?php endif; ?>