<section id="filters">

	<strong>Filter by</strong>

	<a href="<?php echo site_url('/newsroom/'); ?>" class="date-link">Date</a>
	<a href="<?php echo site_url('/newsroom/popular/'); ?>" class="popular-link">Popularity</a>
	<a href="<?php echo site_url('/newsroom/comments/'); ?>" class="comments-link">Comments</a>

	<div class="category-dropdown">

		<?php wp_dropdown_categories( 'show_option_none=Category' ); ?>
		<script type="text/javascript">
			<!--
			var dropdown = document.getElementById("cat");
			function onCatChange() {
				if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
					location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
				}
			}
			dropdown.onchange = onCatChange;
			-->
		</script>

	</div>

</section>