<?php get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">

		<div class="info">
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php the_field('hero_deck'); ?>	
		</div>

	</section>


	<section id="guide-links">
		<div class="wrapper">

			<?php wp_nav_menu( array( 'menu' => 'Guide Links') ); ?>
			<a href="#" class="search-toggle"><img src="<?php bloginfo('template_directory') ?>/images/search-icon-white.svg" alt="Search" /></a>

			<div class="text-search">
				<input type="search" placeholder="Search" />
				<button data-search="next">&darr;</button>
				<button data-search="prev">&uarr;</button>
			</div>

		</div>
	</section>


	<section id="table-of-contents">

		<?php $counter = 1; if(have_rows('table_of_contents')): while(have_rows('table_of_contents')): the_row(); ?>

	    	<?php $post_object = get_sub_field('page'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
	 
			    <div class="content-section order-<?php echo $counter; ?>">
			    	<div class="wrapper">

				    	<div class="info">
				    		<h4>Section </h4>
				    		<h3><?php the_title(); ?></h3>

				    		<h5><?php the_field('table_of_contents_headline'); ?></h5>
				    		<?php the_field('table_of_contents_description'); ?>

				    		<a href="#<?php echo sanitize_title_with_dashes(get_the_title()); ?>" class="btn">
				    			<?php the_field('table_of_contents_cta'); ?>
				    		</a>
				    	</div>

				    	<div class="photo">
				    		<img src="<?php $image = get_field('table_of_contents_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>
			    	</div>

			    </div>

			<?php wp_reset_postdata(); endif; ?>

		<?php $counter++; endwhile; endif; ?>

	    <div class="content-section featured-image cover" style="background-image: url(<?php $image = get_field('table_of_contents_photo'); echo $image['url']; ?>);"></div>

	</section>

	<section id="section-content">

		<?php if(have_rows('table_of_contents')): while(have_rows('table_of_contents')): the_row(); ?>

			<?php $post_object = get_sub_field('page'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

		    	<section id="<?php echo sanitize_title_with_dashes(get_the_title()); ?>" class="guide-section">

					<?php if(have_rows('content')): while(have_rows('content')) : the_row(); ?>
					 
					    <?php if( get_row_layout() == 'section_header' ): ?>
							
					    	<?php get_template_part('partials/guide/section-header'); ?>
							
					    <?php endif; ?>
					 
					<?php endwhile; endif; ?>

					<?php if(have_rows('content')): ?>
						<div class="content">

							<?php while(have_rows('content')) : the_row(); ?>
							 
							    <?php if( get_row_layout() == 'full_width_headline' ): ?>
									
							    	<?php get_template_part('partials/guide/full-width-headline'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'full_width_text' ): ?>
									
							    	<?php get_template_part('partials/guide/full-width-text'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'full_width_inset_photo' ): ?>
									
							    	<?php get_template_part('partials/guide/full-width-inset-photo'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'full_bleed_photo' ): ?>
									
							    	<?php get_template_part('partials/guide/full-bleed-photo'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'two_column_text' ): ?>
									
							    	<?php get_template_part('partials/guide/two-column-text'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'two_column_text_and_photo' ): ?>
									
							    	<?php get_template_part('partials/guide/two-column-text-and-photo'); ?>
									
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'inset_box' ): ?>
									
							    	<?php get_template_part('partials/guide/inset-box'); ?>
									
							    <?php endif; ?>
							 
							<?php endwhile; ?>

						</div>
					<?php endif; ?>


			    </section>

			<?php wp_reset_postdata(); endif; ?>

		<?php endwhile; endif; ?>

		<div class="wrapper back-to-top-wrapper">
			<a href="#hero" id="back-to-top">
				<img src="<?php bloginfo('template_directory') ?>/images/back-to-top.svg" alt="Up Arrow" />
				<span>Back to top</span>
			</a>
		</div>


	</section>

<?php get_footer(); ?>