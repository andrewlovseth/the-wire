<?php get_header(); ?>

	<section id="jobs-header">
		<div class="wrapper">

			<h1><?php the_field('jobs_archive_headline', 'options'); ?></h1>

		</div>
	</section>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('jobs_archive_hero_image', 'options'); echo $image['url']; ?>);">
		
	</section>

	<section id="job-types">
		<div class="wrapper">

			<div id="job-types-wrapper">

				<?php 

				$depts = get_field('jobs_archive_departments', 'options');

				if( $depts ): ?>

					<?php foreach( $depts as $dept ): ?>
					
						<article>
							<div class="featured-image">
								<a href="<?php echo get_term_link( $dept ); ?>" class="dept-toggle" data-dept="<?php echo $dept->slug; ?>">
									<img src="<?php $image = get_field('featured_image', 'dept_' . $dept->term_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>

							<div class="info">
								<h3><a href="<?php echo get_term_link( $dept ); ?>"><?php echo $dept->name; ?></a></h3>
								<a href="<?php echo get_term_link( $dept ); ?>" class="see-all dept-toggle" data-dept="<?php echo $dept->slug; ?>">See all openings</a>

							</div>
						</article>

					<?php endforeach; ?>

				<?php endif; ?>

				<section class="filters">
					<strong>Filter by</strong>

					<?php get_template_part('partials/location-selector'); ?>

					<?php get_template_part('partials/date-selector'); ?>
				</section>


			</div>

		</div>
	</section>


	<section id="job-list">
		<div class="wrapper">

			<div id="loader"><div class="loading"></div></div>

			<h3 class="cat-header"></h3>

			<div id="response">

			</div>

		</div>
	</section>


	<section id="learn-more">
		<div class="wrapper">

			<div class="learn-more-wrapper">

				<h2><?php the_field('jobs_learn_home_headline', 'options'); ?></h2>
				<?php the_field('jobs_learn_more_description', 'options'); ?>

				<a href="<?php the_field('jobs_learn_more_link', 'options'); ?>">Learn more</a>

			</div>

		</div>
	</section>
	
<?php get_footer(); ?>