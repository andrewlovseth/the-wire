<?php get_header(); ?>

<?php $queried_object = get_queried_object(); ?>

	<section id="jobs-header">
		<div class="wrapper">

			<h1><?php echo $queried_object->name; ?> Jobs</h1>

		</div>
	</section>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('featured_image', 'dept_' . $queried_object->term_id); echo $image['url']; ?>);">
		
	</section>


	<section id="job-list">
		<div class="wrapper">

			<?php
				$dept = $queried_object->slug;
				$args = array(
					'post_type' => 'jobs',
					'posts_per_page' => 50,
					'tax_query' => array(
						array(
							'taxonomy' => 'dept',
							'field'    => 'slug',
							'terms'    => $dept,
						),
					),


					'tax' => $dept
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<article>
						<h4><?php the_field('location'); ?></h4>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

						<div class="info">
							<div class="description">
								<?php the_field('short_description'); ?>
							</div>

							<div class="cta">
								<a href="<?php the_permalink(); ?>" class="btn">See Details</a>
							</div>
						</div>
					</article>

				<?php endwhile; ?>

			<?php else: ?>

				<article class="no-results">
					<h3>No job posts found</h3>

					<div class="info">
						<div class="description">
							<p>Check back soon for new listings.</p>
						</div>

					</div>
				</article>

			<?php endif; wp_reset_postdata(); ?>

		</div>
	</section>


	<section id="learn-more">
		<div class="wrapper">

			<div class="learn-more-wrapper">

				<h2><?php the_field('jobs_learn_home_headline', 'options'); ?></h2>
				<?php the_field('jobs_learn_more_description', 'options'); ?>

				<a href="<?php the_field('jobs_learn_more_link', 'options'); ?>">Learn more</a>

			</div>

		</div>
	</section>
	
<?php get_footer(); ?>